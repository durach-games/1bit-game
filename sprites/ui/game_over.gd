extends HFlowContainer

var score = 0

func _ready():
	pass
	
func _process(_delta):
	var players = get_tree().get_nodes_in_group("Player")
	if len(players) == 0:
		visible = true
		
		get_node("Reload").text = "High score: %s\nPress F5 to reload" % score
	else:
		score = players[0].get_score() # hack backup

	if Input.is_action_just_pressed("ui_reload"):
		get_tree().reload_current_scene()

extends Node2D

@export var gridSize:int = 42

@export var timer:float = 5
@export var maxEnemies:int = 500

func spawn_enemies():
	var players = get_tree().get_nodes_in_group("Player")
	if len(players) == 0:
		return
	var player_pos = players[0].global_position
	
	
	var enemies = get_tree().get_nodes_in_group("Enemy")
	var to_spawn = min(20, maxEnemies - len(enemies))
	
	for i in range(0, to_spawn):
		var enemy = load("res://scenes/enemy.tscn")
		var instance2 = enemy.instantiate()
		add_child(instance2)
		
		var r = randf_range(4, 20)
		var theta = randf_range(0, 2 * PI)
		var random_vector = r * Vector2(
			cos(theta), 
			sin(theta)
		)
		instance2.position = player_pos + random_vector * gridSize

func _ready():
	var scene = load("res://scenes/gui.tscn")
	var instance = scene.instantiate()
	add_child(instance)
	
	spawn_enemies()
		

func _process(delta):
	timer -= delta
	if timer < 0.0:
		timer += 5
		spawn_enemies()

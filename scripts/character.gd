class_name Character extends Node2D

@export var health:int = 1000
@export var maxHealth:int = 1000
@export var speedMultiplier:float = 1
@export var invulnerability:float = 1

func is_player():
	return get_parent().get_groups().count("Player") > 0

func set_speed_multiplier(val:float):
	speedMultiplier = val
	
func get_speed_multiplier():
	return speedMultiplier

func damage(amount:int, active:bool = true):
	if invulnerability >= 0:
		return
	health -= amount
	health = clamp(health, 0, maxHealth)
	# print("Ouch %d %d" % [amount, health])
	if health <= 0:
		get_parent().queue_free()
	if amount < 0 && active:
		invulnerability = 1

func _ready():
	pass

func _process(delta):
	invulnerability -= delta
	
	if is_player():
		damage(1, false)

extends ColorRect

func updateMode(a:Player.Ability):
	get_material().set_shader_parameter("mode", int(a))

func _ready():
	var players = get_tree().get_nodes_in_group("Player")
	if len(players) != 0:
		var player:Player = players[0]
		player.abilitySelected.connect(updateMode)
		
		updateMode(player.currentAbility)


func _process(delta):
	pass

extends Label

func _process(delta):
	var players = get_tree().get_nodes_in_group("Player")
	if len(players) != 0:
		text = "High score: %s" % players[0].get_score()

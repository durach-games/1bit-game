extends Container

@export var ability:Player.Ability

var player: Player

@onready var disableSprite : TextureRect = $AbilityImage/Disabled
@onready var selectionSprite : TextureRect = $AbilityImage/Selection

func _ready():
	var players = get_tree().get_nodes_in_group("Player")
	if len(players) != 0:
		player = players[0]
		player.abilitiesChanged.connect(update)
		player.abilitySelected.connect(updateSelection)

func update():
	disableSprite.visible = not player.abilities[ability]

func updateSelection(selectedAbility: Player.Ability):
	selectionSprite.visible = selectedAbility == ability

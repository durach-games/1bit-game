extends Sprite2D

func _ready():
	pass

func _process(delta):
	var character = get_parent().get_parent() as Character
	
	scale.x = lerpf(0.0, 0.937, float(character.health) / float(character.maxHealth))

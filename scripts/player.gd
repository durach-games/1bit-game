class_name Player extends CharacterBody2D

enum Ability { FIRE, ICE }
enum Direction { UP, DOWN, LEFT, RIGHT }

signal abilitiesChanged
signal abilitySelected(selectedAbility: Player.Ability)

@export var currentAbility:Ability = Ability.FIRE

@export var score:int = 0
var scoreTimeout:float = 1

@export var abilities = {
	Ability.FIRE : true, Ability.ICE : true
}

@onready var sprite = $Sprite2D
@onready var character : Character = $Character

var currentAnimation = "idle_down"
var direction = Direction.DOWN
var moveSpeed = 10000

func get_score():
	return score

func getCharacter():
	return character

func fire(dir:Vector2):
	if abilities[currentAbility] == false:
		return

	var scene = load("res://scenes/projectile.tscn")
	var instance:CharacterBody2D = scene.instantiate()
	add_sibling(instance)
	
	var bulletSpeed = 400
	var damage = 400
	if currentAbility == Ability.FIRE:
		bulletSpeed = 800
		damage = 900

	instance.global_position = global_position + dir.normalized() * 40
	instance.velocity = dir.normalized() * bulletSpeed

	var projectile = instance as Projectile
	projectile.rotation = dir.angle() + deg_to_rad(90)
	projectile.set_damage(damage)
	if currentAbility == Ability.ICE:
		projectile.set_freezing(true)
		projectile.set_animation("ice")

func drop(dir:Vector2):
	if abilities[currentAbility] == false:
		return
	
	if currentAbility == Ability.FIRE:
		var scene = load("res://scenes/fire.tscn")
		var instance:Area2D = scene.instantiate()
		add_sibling(instance)
		
		var max_reach = 64
		var l = dir.length()
		dir *= min(l, max_reach) / l
		
		instance.global_position = global_position + dir
		
		abilities[currentAbility] = false
		abilitiesChanged.emit()
	elif currentAbility == Ability.ICE:
		var space_state = get_world_2d().direct_space_state
		
		var mouse_pos = get_global_mouse_position()
		
		var query = PhysicsRayQueryParameters2D.create(global_position, mouse_pos)
		query.exclude = [get_rid()]
		var result = space_state.intersect_ray(query)
		
		if result:
			if result.collider.get_parent() is Water:
				result.collider.get_parent().freeze()
		
				abilities[currentAbility] = false
				abilitiesChanged.emit()

func _ready():
	pass

func _input(event):
	if event is InputEventMouseButton:
		if event.button_index == MOUSE_BUTTON_LEFT and event.pressed:
			var event2 = make_input_local(event)
			
			var dir:Vector2 = event2.position
			fire(dir)
			
func _process(delta):
	sprite.play(currentAnimation)
	
	if Input.is_action_just_pressed("fire"):
		var mouse_position = get_local_mouse_position()
		
		var dir:Vector2 = mouse_position
		fire(dir)
		
	if Input.is_action_just_pressed("drop"):
		var mouse_position = get_local_mouse_position()
		
		var dir:Vector2 = mouse_position
		drop(dir)
		
	if Input.is_action_just_pressed("slot_1"):
		currentAbility = Ability.FIRE
		abilitySelected.emit(currentAbility)
		
	if Input.is_action_just_pressed("slot_2"):
		currentAbility = Ability.ICE
		abilitySelected.emit(currentAbility)
		
	if Input.is_action_just_pressed("slot_next"):
		currentAbility = abs((currentAbility + 1) % Ability.keys().size())
		abilitySelected.emit(currentAbility)
		
	if Input.is_action_just_pressed("slot_previous"):
		currentAbility = abs((currentAbility - 1) % Ability.keys().size())
		abilitySelected.emit(currentAbility)
		
	scoreTimeout -= delta
	if scoreTimeout < 0:
		scoreTimeout = 1
		score += 1

func _physics_process(delta):
	var movement = Vector2.ZERO
	if Input.is_action_pressed("up"):
		movement += Vector2.UP
	if Input.is_action_pressed("down"):
		movement += Vector2.DOWN
	if Input.is_action_pressed("left"):
		movement += Vector2.LEFT
	if Input.is_action_pressed("right"):
		movement += Vector2.RIGHT
	
	velocity = movement.normalized() * moveSpeed * delta
	move_and_slide()
	updateSpriteAnimation()

func updateSpriteAnimation():
	var isMoving = velocity.length() > 0.1
	if isMoving:
		if abs(velocity.x) > abs(velocity.y):
			direction = Direction.RIGHT if velocity.x > 0 else Direction.LEFT
		else:
			direction = Direction.DOWN if velocity.y > 0 else Direction.UP
		
	match direction:
		Direction.DOWN:
			currentAnimation = "walk_down" if isMoving else "idle_down"
		Direction.UP:
			currentAnimation = "walk_up" if isMoving else "idle_up"
		Direction.LEFT:
			currentAnimation = "walk_left" if isMoving else "idle_left"
		Direction.RIGHT:
			currentAnimation = "walk_right" if isMoving else "idle_right"
	

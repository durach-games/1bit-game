extends Area2D

var timeout:float = 10

func _ready():
	pass

func _process(delta):
	var bodies = get_overlapping_bodies()
	
	for body in bodies:
		var node = body as Node2D
		
		for child in node.get_children():
			if child is Character:
				child.damage(100000)

	timeout -= delta
	
	if timeout < 0:
		queue_free()

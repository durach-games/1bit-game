class_name Water extends Node2D

var freezed = false

func freeze():
	print("Freeezing")
	freezed = true
	get_node("Water").hide()
	get_node("Ice").show()
	var shape:CollisionShape2D = get_node("Water/CollisionShape2D")
	shape.disabled = true

func _ready():
	get_node("Water").show()
	get_node("Ice").hide()

func _process(delta):
	pass


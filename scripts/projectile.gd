class_name Projectile extends CharacterBody2D

@export var damage:int = 5
@export var freezing:bool = false
@export var animation:String = "fire"

@onready var sprite: AnimatedSprite2D = $Sprite2D

func set_freezing(val:bool):
	freezing = val
	
func set_damage(val:int):
	damage = val
	
func set_animation(val: String):
	animation = val
	sprite.animation = animation
	sprite.play()

func _physics_process(delta):
	move_and_slide()
	
	var collisionCount = get_slide_collision_count()
	if collisionCount > 0:
		for k in range(0, get_slide_collision_count()):
			var body = get_slide_collision(k)
			var collider = body.get_collider()
			
			if collider is Enemy:
				var enemy = collider as Enemy
				enemy.getCharacter().damage(damage)
				if (freezing):
					enemy.getCharacter().set_speed_multiplier(0.5)
					
				break
				
		queue_free()

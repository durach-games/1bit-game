extends Area2D

@export var healup:int = 100

func _on_area2d_entered(area:Node2D):
	for child in area.get_children():
		if child is Character:
			if (child as Character).is_player():
				child.damage(-healup)
				queue_free()

func _ready():
	monitoring = true
	area_entered.connect(_on_area2d_entered)
	body_entered.connect(_on_area2d_entered)


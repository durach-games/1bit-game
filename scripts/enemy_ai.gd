class_name Enemy
extends CharacterBody2D

var target
var damage:int = 3

@onready var character : Character = $Character

func getCharacter():
	return character

func damage_on_collision():
	for k in range(0, get_slide_collision_count()):
		var body = get_slide_collision(k)
		var collider = body.get_collider()
		
		if collider is Player:
			var player = collider as Player
			player.getCharacter().damage(damage)

func _physics_process(_delta):
	if not target:
		var players = get_tree().get_nodes_in_group("Player")
		if len(players) == 0:
			return
		var randomIndex = randi() % players.size()
		target = players[randomIndex]
	
	var speed = 100
	
	for child in get_children():
		if child is Character:
			speed *= child.get_speed_multiplier()
	
	# velocity = (position - target.position).normalized() * speed
	# Funny repulsive
	velocity = (target.global_position - global_position).normalized() * speed

	move_and_slide()

	damage_on_collision()
		
